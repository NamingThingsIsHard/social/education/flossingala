//! Allows storing and reading language classes.
//!
//! The classes and relative [models](mod@crate::models)
//! are stored as a folder-structure with YAML files therein.
//!
//! This is the generalised structure. `${variable}` is used to denote a variable.
//! Everything else has a fixed name
//!
//! ```sh
//! $language_name/
//! ├── lang  # Contains the language class loaded into models::lang::Language
//! │   ├── $course_name  # There may be multiple courses in a language
//! │   │   ├── $level_name  # There may be multiple levels in a course
//! │   │   │   ├── $chapter_name  # Multiple chapters in a level
//! │   │   │   │   ├── chapter.yaml
//! │   │   │   │   └── units
//! │   │   │   │       └── $unit_name.yaml  # And multiple units in a chapter
//! │   │   │   └── level.yaml
//! │   │   └── course.yaml
//! │   └── language.yaml
//! └── resources  # Contains resources (audio, video, image, text), referenced by other objects
//!     ├── ${resource_group}  # Resource groups are user defined
//!     │   └── ${resource_name}.${filetype}  # As are the resources
//!     └── ${another_resource_group}
//!         └── ${resource_name}.${filetype}
//! ```
//!
//! With this structure, it is possible for multiple people to work on the course in version control
//! and reduce the chance of conflicting edits.

pub mod de;
pub mod field_serde;
pub mod ser;
mod utils;
