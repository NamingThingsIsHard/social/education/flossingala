use inheriters::specialisations;
use serde::{Deserialize, Serialize};

use crate::models::level::Level;

specialisations!(
    #[base(output = false)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    struct CourseBase {
        /// A user facing name for the course
        pub name: String,
        /// A user facing description in markdown
        pub description: String,

        /// Holds the name of the folder this chapter will be written into
        /// It is skipped from (de-)serialisation by serde as it should NOT end up in the YAML
        #[serde(skip)]
        pub simple_name: String,
    }

    /// An intermediate model used to store a course in folder structure
    /// or load from it
    #[inherit(CourseBase)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    pub struct CourseSimple {
        /// Folder names of the levels from which to load full [Level]s from
        pub levels: Vec<String>,
    }

    /// To teach a language, you need to group what it is you want to teach in a course
    ///
    /// A course can be something like "Beginner French" with one level: Beginner,
    /// "Complete Course Of Hindi" with all levels of Hindi,
    /// "Zulu for Business" with one level,
    /// or "Swearing in Russian: All levels" with multiple levels
    #[inherit(CourseBase)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    pub struct Course {
        pub levels: Vec<Level>,
    }
);

impl From<Course> for CourseSimple {
    fn from(course: Course) -> Self {
        CourseSimple {
            levels: course
                .levels
                .iter()
                .map(|level| level.simple_name.clone())
                .collect(),
            name: course.name.clone(),
            description: course.description.clone(),
            simple_name: course.simple_name.clone(),
        }
    }
}

impl Course {
    pub fn new(simple: CourseSimple, levels: Vec<Level>) -> Self {
        Course {
            levels,
            name: simple.name.clone(),
            description: simple.description.clone(),
            simple_name: simple.simple_name.clone(),
        }
    }
}
