use crate::models::unit::Unit;
use inheriters::specialisations;
use serde::{Deserialize, Serialize};

specialisations!(
    #[base(output = false)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    struct ChapterBase {
        /// User-facing name of the chapter
        pub name: String,
        /// User-facing description in markdown
        pub description: String,
        /// Holds the name of the folder this chapter will be written into
        /// It is skipped from (de-)serialisation by serde as it should NOT end up in the YAML
        #[serde(skip)]
        pub simple_name: String,
    }

    /// An intermediate model used to store a chapter in folder structure
    /// or load from it
    #[inherit(ChapterBase)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    pub struct ChapterSimple {
        /// Folder names of the chapters from which to load full [Chapter]s from
        pub units: Vec<String>,
    }

    /// A chapter is group of units used to teach a topic at a certain proficiency level
    ///
    /// Examples include "Introductions" that will teach how introduce the learner to other people
    /// in different circumstances, or "The Present Tense" to teach the most basic tense of the language.
    #[inherit(ChapterBase)]
    #[derive(Clone, Eq, PartialEq, Debug, Deserialize, Serialize)]
    pub struct Chapter {
        pub units: Vec<Unit>,
    }
);

impl Chapter {
    pub fn new(simple: ChapterSimple, units: Vec<Unit>) -> Self {
        Chapter {
            name: simple.name.clone(),
            description: simple.description.clone(),
            simple_name: simple.simple_name.clone(),
            units,
        }
    }
}

impl From<Chapter> for ChapterSimple {
    fn from(chapter: Chapter) -> Self {
        ChapterSimple {
            name: chapter.name.clone(),
            description: chapter.description.clone(),
            simple_name: chapter.simple_name.clone(),
            units: chapter
                .units
                .iter()
                .map(|unit| unit.simple_name.clone())
                .collect(),
        }
    }
}
