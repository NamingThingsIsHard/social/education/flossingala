use crate::models::course::Course;
use inheriters::specialisations;
use isolang::Language as Lang;
use serde::{Deserialize, Serialize};

specialisations!(
    #[base(output = false)]
    struct LanguageBase {
        /// The ISO 639-3 of the language class
        /// For example: `eng` for English
        pub identifier: Lang,
        /// The language used by the learner of this language class
        /// Learning **from** ... to ...
        pub from: Lang,
    }

    /// The simple model to load a full language model
    #[inherit(LanguageBase)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    pub struct LanguageSimple {
        /// Folder names of the courses used to teach this language
        pub courses: Vec<String>,
        /// NOT USED
        /// This field only exists due to the methods in [crate::serde::ser] and [crate::serde::de]
        /// that are made by a macro expecting the model to have a `simple_name` field.
        ///
        /// The name of the folder in which a language class is stored has no importance
        #[serde(skip)]
        pub simple_name: String,
    }

    /// A language class which contains [Course]s
    #[inherit(LanguageBase)]
    #[derive(Clone, Eq, PartialEq, Debug)]
    pub struct Language {
        /// The course object used to teach this language
        pub courses: Vec<Course>,
    }
);

impl Language {
    /// Primarily provided to allow construction from the simple model
    /// because [From::from] only takes one argument
    pub fn new(simple: LanguageSimple, courses: Vec<Course>) -> Self {
        Language {
            identifier: simple.identifier,
            from: simple.from,
            courses,
        }
    }
}

impl From<Language> for LanguageSimple {
    fn from(language: Language) -> Self {
        LanguageSimple {
            identifier: language.identifier,
            from: language.from,
            courses: language
                .courses
                .iter()
                .map(|course| course.simple_name.clone())
                .collect(),
            simple_name: Default::default(),
        }
    }
}
