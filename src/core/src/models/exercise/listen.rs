use serde::{Deserialize, Serialize};

pub mod answer;

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct ListenThenConstructSentenceConfig {
    min_sentence_length: u8,
    max_sentence_length: u8,
}

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct ListenThenConstructSentence {
    pub audio: String,
    pub gen_all: bool,
    pub construct_sentence: bool,
    pub choose_answer: AnswerChoiceConfig,
}

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct AnswerChoiceConfig {
    question: String,
    options: Vec<String>,
}
