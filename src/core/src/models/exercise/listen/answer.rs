use serde::{Deserialize, Serialize};

/// An exercise where one listens to audio and answers a question about what was heard
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct ListenAndAnswer {
    audio: String,
    multiple_choice: bool,
    question: String,
    choice: Vec<String>,
}

pub struct ListenAndAnswerConfig {}
