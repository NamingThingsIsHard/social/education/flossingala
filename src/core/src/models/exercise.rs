use serde::{Deserialize, Serialize};

use listen::answer::ListenAndAnswer;
use listen::ListenThenConstructSentenceConfig;

pub mod listen;

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub enum ExerciseGenerationConfig {
    ListenThenConstructSentence(ListenThenConstructSentenceConfig),
}

/// The equivalent of typing "ExerciseTypeA or ExerciseTypeB or ..."
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub enum Exercise {
    ListenAndAnswer(ListenAndAnswer),
}
