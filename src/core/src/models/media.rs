use serde::{Deserialize, Serialize};

/// Audio, Image, Video, or something else to be used within the language
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct Medium {
    file: String,
}
