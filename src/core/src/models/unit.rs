use crate::models::exercise::{Exercise, ExerciseGenerationConfig};
use inheriters::specialisations;
use serde::{Deserialize, Serialize};

use crate::models::word::{Phrase, Word};
use crate::serde::field_serde::{deserialize_bool, serialize_bool};

/// A way to group exercises on a specific topic
///
/// Examples include "Formal greetings" and "Colloquial greetings" for a chapter on "Introduction",
/// or "Regular Verbs" and "Irregular verbs" for a chapter on "Past Perfect Tense"
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct Unit {
    /// User-facing name for the unit
    pub name: String,
    /// Description of the unit in markdown
    pub description: String,
    /// Data and configuration for exercises
    pub exercises: UnitExercisesConfig,
    /// Name of the file the unit will be stored in **without** the extension
    /// This is skipped by serde as it should **not** end up in the YAML file itself
    #[serde(skip)]
    pub simple_name: String,
}

/// A way to provide either manually created exercises for a unit
/// or automatically generated exercises from data
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub enum UnitExercisesConfig {
    /// Manually created exercises with config on how to present them
    ManualConfig(UnitExercisesConfigManual),
    /// Configuration for automatically generating exercises
    GenerationConfig(UnitExercisesConfigAuto),
}

specialisations!(
    #[base(output = false)]
    pub struct UnitExercisesConfigBase {
        /// Controls how the provided exercises should be presented to the user
        pub options: ExercisesOptions,
    }

    /// Exercises in here are hand-configured by the writer
    #[inherit(UnitExercisesConfigBase)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    pub struct UnitExercisesConfigManual {
        pub exercises: Vec<Exercise>,
    }

    /// Configuration and data to generate exercises from
    #[inherit(UnitExercisesConfigBase)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    pub struct UnitExercisesConfigAuto {
        /// What to use in order to generate exercises
        pub data: UnitExercisesGenData,
        /// Which types of exercises to generate from the data
        pub types: Vec<ExerciseGenerationConfig>,
        /// Tags to use when filtering the global list of exercise types
        ///
        /// The result of that operation can be used to merge with the [types][Self::types]
        pub types_by_tag: Vec<String>,
    }
);

/// Options to configure how exercises will be presented
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct ExercisesOptions {
    /// The exercises will be presented in a randomized order
    /// Accepts the strings "yes" and "no" during serde
    #[serde(
        serialize_with = "serialize_bool",
        deserialize_with = "deserialize_bool"
    )]
    pub randomize: bool,
}

/// All data necessary to generate exercises for a unit
#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct UnitExercisesGenData {
    /// Phrases to be learned in the unit
    pub phrases: Vec<Phrase>,
    /// Words to be learned in the unit
    pub words: Vec<Word>,
}
