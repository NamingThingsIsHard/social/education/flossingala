use crate::models::media::Medium;
use crate::models::note::Note;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct Word {
    pub word: String,
    pub translation: String,
    pub media: Vec<Medium>,
}

#[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
pub struct Phrase {
    pub phrase: String,
    pub translation: String,
    pub alternatives: Vec<String>,
    pub media: Vec<Medium>,
    pub note: Note,
}
