use inheriters::specialisations;
use serde::{Deserialize, Serialize};

use crate::models::chapter::Chapter;

specialisations!(
    #[base(output = false)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    struct LevelBase {
        /// User facing name
        ///
        /// If you can, it's recommended to stick to a standard, like one from
        ///  - Common European Framework of Reference for Languages ([CEFR])
        ///  - American Council on the Teaching of Foreign Languages ([ACTFL])
        ///  - Interagency Language Roundtable ([ILR]) scale
        ///
        /// [CEFR]: https://en.wikipedia.org/wiki/Common_European_Framework_of_Reference_for_Languages
        /// [ILR]: https://en.wikipedia.org/wiki/ILR_scale
        /// [ACTFL]: https://en.wikipedia.org/wiki/American_Council_on_the_Teaching_of_Foreign_Languages
        pub name: String,
        /// User facing description in markdown
        pub description: String,
        /// Holds the name of the folder this chapter will be written into
        /// It is skipped from (de-)serialisation by serde as it should NOT end up in the YAML
        #[serde(skip)]
        pub simple_name: String,
    }

    /// An intermediate model used to store a level in folder structure
    /// or load from it
    #[inherit(LevelBase)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    pub struct LevelSimple {
        pub chapters: Vec<String>,
    }

    /// A level of language proficiency that groups chapters within
    #[inherit(LevelBase)]
    #[derive(Clone, Debug, Eq, PartialEq, Deserialize, Serialize)]
    pub struct Level {
        pub chapters: Vec<Chapter>,
    }
);

impl Level {
    pub fn new(simple: LevelSimple, chapters: Vec<Chapter>) -> Self {
        Level {
            chapters,
            name: simple.name.clone(),
            description: simple.description.clone(),
            simple_name: simple.simple_name,
        }
    }
}

impl From<Level> for LevelSimple {
    fn from(level: Level) -> Self {
        LevelSimple {
            name: level.name.clone(),
            description: level.description.clone(),
            simple_name: level.simple_name.clone(),
            chapters: level
                .chapters
                .iter()
                .map(|chapter| chapter.simple_name.clone())
                .collect(),
        }
    }
}
