use std::path::{Path, PathBuf};

pub fn join(left: &Path, right: &String) -> PathBuf {
    left.join(right)
}

pub fn join_unit_yaml(folder: &Path, filename: &String) -> PathBuf {
    folder.join("units").join(format!("{}.yaml", filename))
}
