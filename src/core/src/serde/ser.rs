use std::fs;
use std::path::Path;

use anyhow::{anyhow, Context};

use crate::models::chapter::{Chapter, ChapterSimple};
use crate::models::course::{Course, CourseSimple};
use crate::models::language::{Language, LanguageSimple};
use crate::models::level::{Level, LevelSimple};
use crate::models::unit::Unit;
use crate::serde::utils::{join, join_unit_yaml};

macro_rules! write_from_simple {
    (
            $fun_name:ident,
            $final_type:ident,
            $simple_type:ident,
            $field:ident,
            $field_writer:ident,
            $field_folder_builder:ident,
            $yaml_filename:literal
        ) => {
        pub fn $fun_name(folder: &Path, model: &$final_type) -> anyhow::Result<()> {
            fs::create_dir_all(folder)
                .context(anyhow!("Could not create folder at {:?}", folder))?;
            let yaml_path = folder.join($yaml_filename);

            let simple_model = $simple_type::from(model.clone());
            let yaml_content = serde_yaml::to_string(&simple_model).context(anyhow!(
                "Could not serialize {} at {:?}",
                stringify!($simple_type),
                &simple_model
            ))?;
            fs::write(yaml_path, yaml_content)?;

            // Write field
            for value in model.$field.clone() {
                let field_folder = $field_folder_builder(&folder, &value.simple_name);
                $field_writer(field_folder.as_path(), &value)?;
            }
            Ok(())
        }
    };
}

write_from_simple!(
    write_language,
    Language,
    LanguageSimple,
    courses,
    write_course,
    join,
    "language.yaml"
);
write_from_simple!(
    write_course,
    Course,
    CourseSimple,
    levels,
    write_level,
    join,
    "course.yaml"
);
write_from_simple!(
    write_level,
    Level,
    LevelSimple,
    chapters,
    write_chapter,
    join,
    "level.yaml"
);

write_from_simple!(
    write_chapter,
    Chapter,
    ChapterSimple,
    units,
    write_unit,
    join_unit_yaml,
    "chapter.yaml"
);

fn write_unit(filename: &Path, unit: &Unit) -> anyhow::Result<()> {
    let parent = filename
        .parent()
        .ok_or(anyhow!("Has no parent {:?}", filename))?;
    fs::create_dir_all(parent)?;
    let unit_str =
        serde_yaml::to_string(unit).context(anyhow!("Cannot serialize unit {:?}", unit))?;
    Ok(fs::write(filename, unit_str)?)
}
