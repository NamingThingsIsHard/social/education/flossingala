//! Methods and modules to help with serializing/deserializing struct fields

use serde::{de, Serializer};

/// Allow serializing a boolean to yes/no
pub fn serialize_bool<S>(boolean: &bool, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    if *boolean {
        serializer.serialize_str("yes")
    } else {
        serializer.serialize_str("no")
    }
}

/// Allow deserializing a boolean from yes/no
pub fn deserialize_bool<'de, D>(deserializer: D) -> Result<bool, D::Error>
where
    D: de::Deserializer<'de>,
{
    let s: &str = de::Deserialize::deserialize(deserializer)?;

    match s {
        "yes" => Ok(true),
        "no" => Ok(false),
        _ => Err(de::Error::unknown_variant(s, &["SI", "NO"])),
    }
}
