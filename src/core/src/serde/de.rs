use std::fs;
use std::path::Path;

use anyhow::{anyhow, Context};

use crate::models::chapter::{Chapter, ChapterSimple};
use crate::models::course::{Course, CourseSimple};
use crate::models::language::{Language, LanguageSimple};
use crate::models::level::{Level, LevelSimple};
use crate::models::unit::Unit;
use crate::serde::utils::{join, join_unit_yaml};

macro_rules! read_from_simple {
    (
            $fun_name:ident,
            $final_type:ident,
            $simple_type:ident,
            $field:ident,
            $field_type:ident,
            $field_reader:ident,
            $field_folder_builder:ident,
            $yaml_filename:literal
        ) => {
        pub fn $fun_name(folder: &Path) -> anyhow::Result<$final_type> {
            let yaml_path = folder.join($yaml_filename);

            let yaml_content = fs::read_to_string(yaml_path.clone())
                .context(anyhow!("Could not read YAML at {:?}", yaml_path.clone()))?;
            let mut simple: $simple_type =
                serde_yaml::from_str(yaml_content.as_str()).context(anyhow!(
                    "Could not read {} at {:?}",
                    stringify!($simple_type),
                    yaml_path.clone()
                ))?;
            simple.simple_name = String::from(
                folder
                    .components()
                    .last()
                    .ok_or(anyhow!("Folder doesn't have a name: {:?}", folder))?
                    .as_os_str()
                    .to_str()
                    .ok_or(anyhow!("Couldn't get last str component of {:?}", folder))?,
            );

            // Read
            let mut $field: Vec<$field_type> = vec![];
            for value in simple.$field.clone() {
                let field_folder = $field_folder_builder(&folder, &value);
                $field.push($field_reader(field_folder.as_path())?)
            }
            Ok($final_type::new(simple, $field))
        }
    };
}

read_from_simple!(
    read_language,
    Language,
    LanguageSimple,
    courses,
    Course,
    read_course,
    join,
    "language.yaml"
);
read_from_simple!(
    read_course,
    Course,
    CourseSimple,
    levels,
    Level,
    read_level,
    join,
    "course.yaml"
);
read_from_simple!(
    read_level,
    Level,
    LevelSimple,
    chapters,
    Chapter,
    read_chapter,
    join,
    "level.yaml"
);

read_from_simple!(
    read_chapter,
    Chapter,
    ChapterSimple,
    units,
    Unit,
    read_unit,
    join_unit_yaml,
    "chapter.yaml"
);

fn read_unit(file_path: &Path) -> anyhow::Result<Unit> {
    let mut unit: Unit = serde_yaml::from_str(fs::read_to_string(file_path)?.as_str())
        .context(anyhow!("Could not deserialize unit {:?}", file_path))?;
    let stem_os_str = file_path
        .file_stem()
        .ok_or(anyhow!("Couldn't get filename from {:?}", file_path))?;
    let stem = stem_os_str
        .to_str()
        .ok_or(anyhow!("Cannot convert to &str {:?}", stem_os_str))?;
    unit.simple_name = stem.to_string();
    Ok(unit)
}
