//! Models for storing and reading languages
//!
//! There is no clean separation between the serialisation and deserialisation yet.
//! Therefore, the models are tightly coupled; something to improve on later.
//!
//! Because courses are stored in a folder structure (described in [serde](mod@crate::serde)),
//! the final model cannot be loaded immediately, but has to be done in multiple steps,
//! reflected by interim models.
//! Models loaded like this follow this structure
//!
//! ## Multistage loaded models
//!
//! `ModelBase -> ModelSimple -> Model`, where `->` denotes inheritance implemented by [`inheriters`].
//!
//! ### ModelBase
//!
//! This is the base struct containing fields common to the subclasses.
//! It has no further function and shouldn't end up in the compiled output
//!  ([`inheriters`] supports `#[base(output = false)]` since v0.5.0).
//!
//! ### ModelSimple
//!
//! This struct contains the fields stored directly in YAML.
//! It holds no complex types and is therefore "simple".
//! One can think of it as the representation of the YAML structure in a model.
//!
//! The simple model is used to dereference the additional YAML files that have to be loaded.
//! See [`CourseSimple`] for example that has the `modules: Vec<String>` field to list the names
//! of the modules that have to be loaded to create the final model.
//!
//! ### Model
//!
//! This is the final "complex" model created from the "simple" model.
//! That's mostly done by loading a file in a separate location.
//! The loaded file can of course also end up just being a simple representation of a complex model
//! and require further loading of fields.
//!
//! The conversion from `Model` to `ModelSimple` is best implemented by using the [`From`] trait.

pub mod chapter;
pub mod course;
pub mod exercise;
pub mod language;
pub mod level;
pub mod media;
pub mod note;
pub mod unit;
pub mod word;
