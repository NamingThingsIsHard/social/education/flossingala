use flossingala::models::language::Language;
use std::path::PathBuf;

use flossingala::models::chapter::Chapter;
use flossingala::models::course::Course;
use flossingala::models::level::Level;
use flossingala::models::unit::{
    ExercisesOptions, Unit, UnitExercisesConfig, UnitExercisesConfigManual,
};
use isolang::Language as IsoLang;
use temp_testdir::TempDir;

use flossingala::serde::de::read_language;
use flossingala::serde::ser::write_language;

#[test]
fn test_read_simple_language() {
    let mut language_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    language_dir.push("tests/resources/languages/french_complete/lang");
    let language = read_language(&language_dir).expect("A language must've been read");
    let expected_language = get_simple_french();
    assert_eq!(language, expected_language);
}

#[test]
fn test_write_simple_language() {
    let language_dir = TempDir::default().permanent();
    println!("Language_dir {}", language_dir.display());
    let language = get_simple_french();
    write_language(&language_dir, &language).expect("A language couldn't be written");
    let resulted_language =
        read_language(&language_dir).expect("Written language couldn't be read");

    // Ensure the written language can be read
    assert_eq!(language, resulted_language);
}

fn get_simple_french() -> Language {
    Language {
        identifier: IsoLang::Fra,
        from: IsoLang::Eng,
        courses: vec![Course {
            name: "French Complete".to_string(),
            description: "Francais from beginner to hero".to_string(),
            simple_name: "french_complete".to_string(),
            levels: vec![Level {
                name: "Beginner".to_string(),
                description: "Learn how to speak French at a A1-A2 level".to_string(),
                simple_name: "beginner".to_string(),
                chapters: vec![Chapter {
                    name: "Introduction".to_string(),
                    description: "A short and tasty introduction to the French language"
                        .to_string(),
                    simple_name: "introduction".to_string(),
                    units: vec![Unit {
                        name: "Greetings".to_string(),
                        description: "How to introduce yourself".to_string(),
                        simple_name: "greetings".to_string(),
                        exercises: UnitExercisesConfig::ManualConfig(UnitExercisesConfigManual {
                            options: ExercisesOptions { randomize: false },
                            exercises: vec![],
                        }),
                    }],
                }],
            }],
        }],
    }
}
