[flossingala] is a language learning suite which is a fork of LibreLingo which in turn is an opensource
alternative to DuoLingo
 
It's composed of a [library][flib] which provides the base functionality upon which to build 
language learning apps and a [desktop app][fdesktop].
 
# The name
 
A mix of either:
 - FLOSS (Free Libre OpenSource Software) + lingua + random ending
 - FLOSS (Free Libre OpenSource Software) + Lingala (a Bantu language in D.R Congo)
 
Basically an attempt at making the name less romance language-y

[flib]: src/core
[fdesktop]: src/desktop
[flossingala]: https://gitlab.com/NamingThingsIsHard/social/education/flossingala
